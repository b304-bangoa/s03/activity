package com.zuitt.WDC043_S3_A1;

import java.util.Scanner;

public class FactorialNumber {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.println("Input an integer whose factorial will be computed");

        int answer = 1;

        try{

            int num = in.nextInt();

            /* Using while loop

        if(num >= 0) {
            while( counter <= num ){
                answer = answer * counter;
                counter++;
            }
            System.out.println("The factorial of " + num + " is " + answer);
        } else {
            System.out.println("Entered a negative integer. Please enter a positive integer.");
        }*/

            //for loop
            if(num >= 0) {
                for(int counter = 1; counter <= num; counter++ ) {
                    answer=answer*counter;
                }

                System.out.println("The factorial of " + num + " is " + answer);
            } else {
                System.out.println("Entered a negative integer. Please enter a positive integer.");
            }
        }
        catch (Exception e) {
            System.out.println("Error. Please enter a positive integer.");
            e.printStackTrace();
        }




    }
}
